`prepare_for_web.py`
####################

This is the original Python-based implementation of the tool for processing
images taken using a Canon EOS camera using the "Adobe RGB" color space and
making them suitable for consumption in web browers/image sharing sites without
all the colours getting washed out.

The script was originally hacked together to be used with Python 2. Support was
later added to also alternatively use Python 3 instead.

## Requirements
* Python 2 or 3
* PILLOW (5.2.0 tested)

## Install Instructions
The script and the ICC profile need to be stored alongside each other in the same directory,
in a location that is on the PATH.

For the Python 2 version, a hardcoded location is used (i.e. "C:\Users\Joshua\cmdutils\").
You will need to manually modify this to point to where you've put the files.

For the Python 3 version, the files only need to be in the same folder as each other.

## History
The first version of this was created in 2017, sometime between April and August.
It then received a few minor updates - first to ensure that EXIF data was being
preserved, then later, to add Python 3 support.

In late December 2018, the code was significantly updated and cleaned up in an attempt
to optimise the speed of processing, as it was taking a good 4-5 minutes to process a
folder of ~500-800 JPG's on a modern rig. Work was also done to improve the console
output so that it was easier to monitor progress and to know how many files were being
processed.


