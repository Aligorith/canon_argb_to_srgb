extern crate image;
extern crate lcms2;
extern crate rayon;
extern crate indicatif;

extern crate exiv2_copy_rs;

use image::{Rgb};
use lcms2::*;

use indicatif::{ProgressBar, ProgressStyle, ParallelProgressIterator};
use rayon::prelude::*;

use std::env;
use std::fs;
use std::path::Path;

/* ********************************************** */
/* Constants */

/* Maximum number of matching files to report */
const PREVIEW_LIMIT: usize = 10;

/* ******************************************* */
/* Performance Timing
 *
 * Copied from image-rs/examples/scaleup.rs
 * TODO: Move this to its own file
 */

use std::fmt;
use std::time::{Duration, Instant};

struct Elapsed(Duration);

impl Elapsed {
	fn from(start: &Instant) -> Self
	{
		Elapsed(start.elapsed())
	}
	
/*
	fn from_secs_f64(secs: f64) -> Self
	{
		Elapsed(Duration::from_secs_f64(secs))
	}
	
	fn as_secs_f64(&self) -> f64
	{
		self.0.as_secs_f64()
	}
*/
}

impl fmt::Display for Elapsed {
	fn fmt(&self, out: &mut fmt::Formatter) -> Result<(), fmt::Error> {
		match (self.0.as_secs(), self.0.subsec_nanos()) {
			(0, n) if n < 1000 => write!(out, "{} ns", n),
			(0, n) if n < 1000_000 => write!(out, "{} µs", n / 1000),
			(0, n) => write!(out, "{} ms", n / 1000_000),
			(s, n) if s < 10 => write!(out, "{}.{:02} s", s, n / 10_000_000),
			(s, _) => write!(out, "{} s", s),
		}
	}
}

/* ********************************************** */
/* Filename Handling */

/* Check whether the given file needs to be processed */
fn should_process_file(filename: &str) -> bool
{
	// TODO: Split out the last part of the path if these conditions don't hold
	assert_eq!(filename.contains("/"), false);
	assert_eq!(filename.contains("\\"), false);
	
	/* Ideally, this would check the exif data too,
	 * but in practice, just checking the start/end
	 * of the name is enough. Case Sensitive.
	 */
	filename.starts_with("_MG_") && (filename.ends_with(".JPG") || filename.ends_with(".jpg"))
}

/* Get new (cleaned up) file name to save the result to
 * < filepath: The original filepath is expected to be of the form: "<path>/<to>/_MG_####.JPG"
 * > returns: "IMG_####.JPG"
 *
 * Note: Much of the additional complexity stems from the need to handle
 *       paths that aren't in the current directory.
 */
fn get_filename_for_result(filepath: &str) -> String
{
	let path = Path::new(filepath);
	//assert!(!path.exists() || path.is_file());  // <--- This occassionally causes crashes when doing long lists
	
	// Get the filename part of the path
	let filename = match path.file_name() {
		Some(v) => v.to_str().unwrap(), // was OsStr
		None => {
			eprintln!("ERROR: No filename in '{}'", filepath);
			filepath
		}
	};
	
	// Construct a new filename that replaces the leading underscore
	let new_filename = vec!["I", &filename[1..]].join("");
	
	// Construct a new path that includes the filename
	let new_path = path.with_file_name(new_filename);
	assert_eq!(new_path.parent(), path.parent());
	
	// Return a new path string with the new filename substituted
	return String::from(new_path.display().to_string()); // XXX 
}


#[cfg(test)]
mod tests {
	use ::*;
	
	#[test]
	fn test_filename_filtering()
	{
		// Yes
		assert_eq!(true, should_process_file("_MG_3210.JPG"));     // Standard case
		
		assert_eq!(true, should_process_file("_MG_329.JPG"));      // Note: Shouldn't happen, but we should just handle it
		assert_eq!(true, should_process_file("_MG_329a.JPG"));     // Why not...?
		assert_eq!(true, should_process_file("_MG_329-002.JPG"));  // Why not...?
		
		// No
		assert_eq!(false, should_process_file("_MG_3210.jpg"));    // Lowercase = Not Canon raw
		assert_eq!(false, should_process_file("_MG_3210.cr2"));    // Canon RAW file - Cannot handle this
		assert_eq!(false, should_process_file("IMG_3210.JPG"));    // Uppercase = Exported + Fixed
		assert_eq!(false, should_process_file("Readme.txt"));      // Accessory file - Ignore
	}
	
	#[test]
	fn test_output_filename_fixes()
	{
		// Note: Need to use Path's here to do that comparison, so that the forward/backslash issue is not a problem on Windows
		// Otherwise, we'd need to use a different set of tests on Windows vs all Posix's
		assert_eq!(Path::new("root/path/IMG_3210.JPG"),
				   Path::new(get_filename_for_result("root/path/_MG_3210.JPG").as_str()));
		
		assert_eq!(Path::new("./IMG_3210.JPG"),
				   Path::new(get_filename_for_result("./_MG_3210.JPG").as_str()));
		
		assert_eq!(Path::new("IMG_3210.JPG"),
				   Path::new(get_filename_for_result("_MG_3210.JPG"). as_str()));
	}
}

/* ********************************************** */
/* Argument / File Handling */

/* Find filenames in given directory */
// XXX: Return Paths? Boxed?
fn find_images(files: &mut fs::ReadDir) -> Vec<String>
{
	let mut result: Vec<String> = Vec::new();
	
	for dir_entry in files {
		if let Ok(entry) = dir_entry {
			let path = entry.path();
			
			if Path::new(&path).is_dir() {
				// Directory...  Recurse and process those too
				// TODO: Introduce some exclusions
				println!("    Recursively checking subfolder: '{}'", path.display());
				let subdir_string = path.display().to_string();
				if let Ok(mut files_iter) = fs::read_dir(subdir_string.as_str()) {
					result.append(&mut find_images(&mut files_iter));
				}
			}
			else {
				// File - Check if the filename is one we're interested in
				// path.file_name() is OsStr
				if let Some(filename) = path.file_name().unwrap().to_str() {
					if should_process_file(filename) {
						result.push(path.display().to_string()); // XXX
					}
				}
			}
		}
	}
	
	return result;
}

/* From the program's arguments, get a list of filenames to process */
// TODO: Optimise this the directory crawling what other Rust programs do
fn filenames_from_args() -> Vec<String>
{
	// Get current path as a string, to display when reporting when path lookups in current directory fail
	let current_path = &env::current_dir().unwrap();
	let current_path_string = Path::new(current_path).display();
	
	// "files" is the list of paths to return from this function
	let mut result: Vec<String> = Vec::new();
	
	// Note: skip(1) to ignore the program's name
	let args: Vec<String> = env::args().skip(1).collect();
	if args.len() > 0 {
		// The args will specify which files / directories to process
		println!("Checking supplied paths...");
		for arg in args {
			if let Ok(mut files_iter) = fs::read_dir(&arg) {
				// Directory - Find everything interesting there to convert
				result.append(&mut find_images(&mut files_iter));
			}
			else if arg.chars().all(char::is_numeric) {
				// Partial filename - just the digits, for convenience
				// NOTE: Assumes that this is in the current directory
				let full_filename = format!("_MG_{}.JPG", arg);
				let path = Path::new(full_filename.as_str());
				if path.exists() {
					result.push(path.display().to_string()); // XXX
				}
				else {
					eprintln!("    ERROR: File '{}' doesn't exist", full_filename);
				}
			}
			else {
				// Explicit path
				let path = Path::new(arg.as_str());
				if path.exists() {
					result.push(path.display().to_string()); // XXX
				}
				else {
					if path.is_absolute() {
						eprintln!("    ERROR: File '{}' doesn't exist", arg);
					}
					else {
						eprintln!("    ERROR: File '{}' doesn't exist in '{}'", arg, current_path_string);
					}
				}
			}
		}
	}
	else {
		// Process the files in the current directory
		println!("Checking current directory: '{}'", current_path_string);
		if let Ok(mut files_iter) = fs::read_dir(current_path) {
			result.append(&mut find_images(&mut files_iter));
		}
	}
	
	return result;
}

/* ********************************************** */
/* Color Profile Context Data */

/* Container for color profile data */
struct ColorProfileData {
	/* sRGB profile (Profile::new_srgb()) */
	srgb : Profile,
	/* Adobe RGB (Profile::new_icc(argb)) */
	argb : Profile,
}

impl ColorProfileData {
	fn new() -> ColorProfileData
	{
		/* Load the AdobeRGB profile (should be baked into the binary) */
		let icc_file = include_bytes!("AdobeRGB.icc");
		
		let srgb_profile = Profile::new_srgb();
		let adobe_profile = Profile::new_icc(icc_file).expect("Adobe RGB profile couldn't be loaded");
		
		/* Create and return new profile */
		ColorProfileData {
			srgb : srgb_profile,
			argb : adobe_profile,
		}
	}
}

/* ********************************************** */
/* Color Processing */

/* Perform color conversions on a file */
fn process_image(cs_ctx: &ColorProfileData, in_file: &str, out_file: &str)
{
	match image::open(in_file) {
		// image_container is a DynamicImage (i.e. an enum of several different image types)
		// which we cannot actually iterate over to modify
		Ok(mut image_container) => {
			// image_data is an ImageBuffer (of type RgbImage<u8>), which we can then modify
			if let Some(image_data) = image_container.as_mut_rgb8() {
				// Obtain color transform for this image
				// XXX: This should be in the context?
				let t = Transform::new(&cs_ctx.argb, PixelFormat::RGB_8,
									   &cs_ctx.srgb, PixelFormat::RGB_8,
									   Intent::Perceptual).unwrap();
				
				// Copy all pixel data into separate arrays, then put them back
				// in a separate loop. In case some minor optimisations are
				// possible for some of the copying that was happening.
				let input: Vec<(u8, u8, u8)>
					= image_data
						.pixels()
						.map(|pixel| (pixel[0], pixel[1], pixel[2]))
						.collect();
				
				let mut out: Vec<(u8, u8, u8)> 
					= (0..input.len())
						.into_par_iter()
						.map(|_| (0 as u8, 0 as u8, 0 as u8))
						.collect();
				
				t.transform_pixels(&input, &mut out);
				
				image_data
					.pixels_mut().zip(out.iter())
					.for_each(|(pixel, color)| {
						*pixel = Rgb([color.0, color.1, color.2]);
					});
				
				// Save resulting image to out_file
				image_data.save(out_file).unwrap();
				
				// Copy over missing EXIF data
				exiv2_copy_rs::copy_exif_data(in_file, out_file);
			}
			else {
				eprintln!("ERROR: {} couldn't be converted to 8-bit RGB image for editing", in_file);
			}
		},
		Err(e) => {
			eprintln!("ERROR: {} couldn't be loaded for processing - {}", in_file, e);
		}
	}
}

/* ********************************************** */

fn main()
{
	/* Handle args + Find Files */
	let paths = filenames_from_args();
	let n = paths.len();
	
	println!("\nFound {} images", paths.len());
	if n == 0 {
		// Silently exit if nothing found
		std::process::exit(0);
	}
	else if n < PREVIEW_LIMIT {
		// Show what we found if there aren't too many
		// (Use this when debugging, to ensure we're culling the right ones)
		println!("");
		for path in &paths {
			println!("    {}", path);
		}
	}
	else {
		// Don't print all of them (as then we miss the full count)
		// (Useful when doing production exports on datasets with 100-600 shots)
		println!("");
		for path in paths.iter().take(PREVIEW_LIMIT) {
			println!("    {}", path);
		}
		println!("    + [... {} more ...]", n - PREVIEW_LIMIT);
	}
	
	/* Process images */
	println!("\n\nProcessing {} files...", n);
	
	// Custom styling for the indicatif progressbar
	let pb = ProgressBar::new(n as u64);
	pb.set_style(ProgressStyle::default_bar()
		.template("Converting: [{elapsed_precise}] [{bar:40.cyan/blue}] {pos:>4}/{len:4} ({eta})")
		.progress_chars("#>-"));
	
	let total_start_time = Instant::now();
	//let mut perf_data: Vec<Elapsed> = Vec::new();
	
	paths
		.par_iter()
		.progress_with(pb)
		.for_each(|filename| {
			// ColorProfileData cannot be shared across threads due to LCMS's GlobalContext,
			// which cannot be shared across threads for obvious reasons.
			//
			// However, there doesn't seem to be a negative impact of creating this
			// context once per task. Perhaps this is because all the profiles are
			// in-memory already (i.e. baked into the binary), so there's no File IO
			// overheads to worry about.
			let cs_ctx = ColorProfileData::new();
			
			//let file_start_time = Instant::now();
			process_image(&cs_ctx, filename, get_filename_for_result(filename).as_str());
			//perf_data.push(Elapsed::from(&file_start_time));
		});
		
	let total_time = Elapsed::from(&total_start_time);
	
	// Report timing
	// let average_time_in_secs 
	// 	= perf_data.iter().map(|d| d.as_secs_f64()).sum::<f64>() / (perf_data.len() as f64);
	// println!("\n\nAverage Time per File: {}", Elapsed::from_secs_f64(average_time_in_secs));
	
	println!("Total Time Taken: {}", total_time);
}

