Rust Version (cats-rs)
======================

Compiling/Installing
--------------------

Currently, this project has only been compiled/tested on Windows 10. Your mileage on other platforms
will vary (and will require some manual work to get working).

Compile in a MSVC shell using:
```$ cargo build --release```

Install system-wide (again in MSVC shell) using:
```$ cargo install```


Usage
-----

* 1. `cd` to a folder full of raw aRGB images to process

* 2a. `$ cats-rs` to process all images in this folder and its subfolders
* 2b. `$ cats-rs 1234 5678 ....` - enter the sequence numbers of individual images to process, separated by spaces.
* 2c. `$ cats-rs filename1.jpg filename.jpg` - enter the filenames/paths to process, separated by spaces.


Dynamic vs Static Libs
-----------------------
Currently, it is necessary to manually copy the exiv2.dll from the `lib-exiv2-copy/exiv2_copy_rs/bin`
folder to `~/.cargo/bin`. This is because the precompiled binaries for exiv2 used only support "shared"
(i.e. dynamic) linking. If static linking is desired, the exiv2 binaries in lib-exiv2-copy will have
to be recompiled from the exiv2 sources and then dumped in the tree.


Roadmap/Todo's:
---------------

* **Restore calculation of average time per image**
** This was removed when switching to Rayon due to the "mutable borrow + Fn closure" error
   noted in bf06b2ec069d946f5493449e811f474e3d894c9c

* **Further optimisations of per-image processing time**
** This is currently even slower than what Python (ostensibly a "scripting" language) can manage.
   Albeit, the Python code is actually using the Pillow library here (which is written in C) for the
   heavy lifting of its image loading + processing, but Rust's performance here still leaves lots to
   be desired.

* **Rules for determining what subdirectories should be skipped**

* **Help text when running with `-h` or `--help`**

* **Automatically copy exiv2.dll to the install directory**


