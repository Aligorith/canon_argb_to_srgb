Canon "Adobe RGB" to "sRGB" Converter
=====================================

Convert JPEG's taken using the "Adobe RGB" color space on Canon EOS DSLR's
to sRGB color space so that they can be uploaded to photo sharing sites
(e.g. Google Photos) without losing the embedded color profile and looking
washed out. This also fixes the file names to replace the underscore prefix
(i.e. "`_MG_####.JPG`" -> "`IMG_####.JPG`").

The resulting binary is called `prepare_for_web` or alternatively, `cats-<lang>`
(e.g. `cats-rs`)

About
-----

This folder contains the msvc 2017 64-bit Windows build of Version 1.0 of cats-rs.

Simply copy the `cats-rs.exe` and `exiv2.dll` to somewhere that can be used to launch
the binary from "everywhere" (i.e. somewhere on PATH).

Usage
-----

1. `cd` to folder containing images to convert
2. `cats-rs` to run the script on all images
3. `cats-rs 1234 5678 ....` (where "1234" and "5678" are the sequence numbers for images to affect)


History + Credits
-----------------

* *Original Author*:  *Joshua Leung* (aligorith@gmail.com) - 2017 - 2020
* *Original Repo*:    https://bitbucket.org/Aligorith/canon_argb_to_srgb

