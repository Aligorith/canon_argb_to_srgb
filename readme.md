Canon "Adobe RGB" to "sRGB" Converter
=====================================

Convert JPEG's taken using the "Adobe RGB" color space on Canon EOS DSLR's
to sRGB color space so that they can be uploaded to photo sharing sites
(e.g. Google Photos) without losing the embedded color profile and looking
washed out. This also fixes the file names to replace the underscore prefix
(i.e. "`_MG_####.JPG`" -> "`IMG_####.JPG`").

The resulting binary is called `prepare_for_web` or alternatively, `cats-<lang>`
(e.g. `cats-rs`)


Directory Structure
-------------------

* **python/** - Python script version (`prepare_for_web.py`)
  See the accompanying readme for info on how to install + use.

* **rust/** - Rust version (`cats-rs`)
  This is the source code for the "cats-rs" Rust version of the tool.

  Compile using "`cargo build --release`" from a MSVC shell (for Windows. Other platforms
  not officially supported).

* **rust/lib-exiv2-copy** - Submodule linking to "exiv-copy-rs" crate's repo
  https://bitbucket.org/Aligorith/exif2-copy-rs/

  Note: To clone this repo with this submodule resolved, clone by doing:
        ```$ git clone --recurse-submodules https://bitbucket.org/Aligorith/canon_argb_to_srgb```

History + Credits
-----------------

* *Original Author*:  *Joshua Leung* (aligorith@gmail.com) - 2017 - 2020
* *Original Repo*:    https://bitbucket.org/Aligorith/canon_argb_to_srgb

